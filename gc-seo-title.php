<?php
/**
 * Plugin Name: Grant Cardone Shows SEO Title
 * Plugin URI: https://bitbucket.org/grantcardone/grant-cardone-shows-seo-title
 * Description: Changes the SEO Title for Grant's shows only
 * Version: 1.1.1
 * Author: Elvis Morales
 * Author URI: https://twitter.com/n3rdh4ck3r
 * Requires at least: 3.5
 * Tested up to: 4.2.2
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

add_filter( 'wp_title', 'gcsst_check_if_grant_show', 99, 1 );

function gcsst_check_if_grant_show( $title ) {

	$is_grant_show = has_category( array( 260, // CardoneZone
		6, // The G&E Show
		262, // Young Hustlers
		7, // Power Players
		263, // Confessions of an Entrepreneur
		66, // Grant Rants
		211 // Cardone Quotes
		)
	);

	if ($is_grant_show)
		return str_replace( '#WhatEverItTakesNetwork', '#GrantCardoneTV', $title );
	else 
		return $title;

}